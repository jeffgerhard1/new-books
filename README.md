# New books
## A set of policies, code, and instructions for adding new books to the digitization workflow.

This README file contains an overview of the contents of this repository. The links to individual notebooks will only work in a Jupyter environment. See the [setup](setup.md) file for more information.

## General outline

1. In Alma Analytics, generate a list of books to pull (see [directions](https://docs.google.com/document/d/1VhSD0SAzN-BXsZcVm-jDeJE949jFXijdOtz4UW-poak/edit?usp=sharing "Google Doc with directions for Alma Analytics")).
    - Run [__"Digitization criteria" notebook__](01-digitization-criteria.ipynb) to parse the Analytics list based on more detailed criteria.
2. Add the resulting list to a [spreadsheet of books to be pulled](https://docs.google.com/spreadsheets/d/17TSXZlVEXiqYnHnpJy_0ESDGVFQ5BDoZ-attrj_3Q7M/edit#gid=553674154 "Google Sheet for book pulling").
    - Also add the bib-to-item lookup table
    - This could be further automated
3. Pull books.
    - We have a tablet computer that can be taken to stacks for book-pulling
    - Students could use their own laptops if they want
    - This could also work on a phone but not easily!
4. After pulling, scan in barcodes on the second worksheet of the spreadsheet above.
    - The datasheet should autopopulate with info for checking IA
    - Check IA status and mark whether or not to continue
5. Run the [__"Barcodes to Database" notebook__](02-barcodes-to-database.ipynb) to read the sheet and:
    - Create a list of barcodes to send to Alma to change location
    - Generate unique identifiers and then add bib and item data to our database.
6. Print pullslips for the books we want to scan
7. Use SRU to pull bib records from Alma and then send them to IA
8. Put books on scanning shelves

***

#### Introduction to the spreadsheet

In addition to the notebooks and code in this repo, the workflow relies heavily on a ["Book-pulling; checking; adding to database and IA" spreadsheet](https://docs.google.com/spreadsheets/d/17TSXZlVEXiqYnHnpJy_0ESDGVFQ5BDoZ-attrj_3Q7M/edit#gid=553674154). The spreadsheet functions as a way to manage data entry, track progress of different items, and store and manipulate data that can go into our digitization database.

There are several worksheets. The first one is designed for pulling books from stacks, and includes checkboxes for statuses like "Pulled", "Missing", and "Rejected."

The second worksheet is designed for processing books after pulling. This is where a user can scan in barcodes for books and check the books against archive.org. Then, an administrator can run code to track the status of the books as they are prepared for scanning. 

A third worksheet is intended for printing sheets to scan books in the stacks, for those who prefer paper to a screen.

There are also set-up worksheets: a lookup table for bib and item info, and some configuration settings for the spreadsheet.

***

## Detailed outline

### PART ONE: pulling books

_This section is managed with the first notebook (["Digitization criteria"](01-digitization-criteria.ipynb))_

1. Gather a list of possible books to scan:
    1. Apply criteria via Alma Analytics
    2. Parse the output for more sophisticated analysis. In the notebook, this is accomplished with Python. Alternatives might include working in spreadsheets, using a tool like Open Refine, or generating more complex queries in Alma Analytics. In any case, we need to check for books that:
        1. have multiple copies and at least one circ
        2. are not in our database already
        3. are not part of our Open Library load from Fall 2018
2. Pull books 
    1. If desired, use the ["print-sheet" notebook](03-print-sheet-for-pulling-books.ipynb) to generate a short list of books to pull
    1. Use the book-pulling spreadsheet for guidance and pull books from stacks
        1. Mark any rejections or missing books on sheet
    1. choose any copy (most suitable to scan)
    2. confirm criteria especially U.S. publisher
    3. (see also some [general guidelines for book-pulling](https://drive.google.com/a/georgetown.edu/file/d/1OBhewmmxbYMY7GdwSiIgn_J2ncdt35Nv/view?usp=sharing))
3. Scan book barcodes into the "barcodes" worksheet of the spreadsheet
4. Manually examine books and check archive.org for duplication
    1. Watch for any cataloging errors
    1. Utilize spreadsheet functionality to launch advanced searches on archive.org for titles with mediatype "texts"
        2. If there are many matches, save the match information
        3. More directions available [in this doc](https://docs.google.com/document/d/1_JMZQz0BTyd590ZVC-Q9w8DA9-WeYikt7Ya5bYzUDFc/edit?usp=sharing)
    3. If already lending via archive.org:
        1. Mark the bib record somehow? __(not implemented)__
        2. Make a digital representation for the lending version __(not implemented)__

### PART TWO: Updating Alma, adding the pulled books to the database, uploading MARC records to the Internet Archive

_This section is managed with the second notebook (["Barcodes-to-database"](02-barcodes-to-database.ipynb))_

1. Update Alma location for books we want to scan
    1. Export a list from the spreadsheet using the second notebook
    2. Import as a set in Alma
    3. Run a job in Alma to give the items a temporary location of DIGT
2. Match up the pulled book barcodes with the spreadsheet originally exported from Alma
    1. This will compile together any metadata we need including things like volume information for items
    3. Also generates UIDs.
3. Add this information to the database
    1. Requires admin access and the VPN
    1. (if possible capture for event history)
4. Mark the new items with "print pullslip" in the database
5. Print pullslips from Access
    1. In the future maybe we can just generate them here via code
4. Extract MARC(XML) records from Alma
    1. This step is not strictly necessary. The IA can access our catalog via Z39.50 and extract MARC records.
        1. However, we still need to send them a list of UIDs and bib numbers. After doing all that work, it's just as easy to upload the MARC records ourselves.
    2. For now, we can do this Via SRU
        1. (_alternatives: MarcEdit, API, Z39.50_)
    3. Lightly clean the MARCXML records (e.g., the AVA fields generated by Alma, LOCAL fields)
        1. There is a bug in the archive.org derive from MARCXML job, so we have to delete any 264 fields beyond the first.
    4. Save these with the same name as the UIDs (like name_of_book_marc.xml)
    5. Upload them to IA with required IA fields like collection, mediatype, noindex, etc.
    6. Add `globallibraries` collection to these items
        1. This is a marker for the Gold Package project, I think.
5. Mark the database again with the IA_metadata_sent status? __(not implemented)__


### CLEAN-UP

1. For any books that we won't scan:
    1. Return to stacks
    3. Mark in database and/or bib records as rejected and a reason __(not implemented)__