# Changelog

## [0.7.2] - 2020-02-26

### Changed

* Revised the "manual upload" notebook to reflect API returns JSON now instead of XML (this may be a local setting that changed?)
* Added more places to refresh the database per [issue 1](https://bitbucket.org/jeffgerhard1/new-books/issues/1/add-database-refresh)



## [0.7.1] - 2020-01-24

### Added

* Some code to hopefully update the database, based on new concerns about IA metadata gathering following the revised IA tasks API change in 12/2019

## [0.7] - 2020-01-22

### Added

* New code to allow any user who has access to the departmental IA account to add the globallibraries collection

## [0.6] - 2019-10-31 🦇

### Added

* Some standalone scripts for uploading one-off records to the Internet Archive

### Changed

* Updated MARCXML edits to allow the multiple 264s again.
    * This seemed to work out fine when testing on Scribe3 software.
* Tweaked credentials handling to add in more robust pathlib features.

## [0.5.1] - 2019-07-12

* Tweak to include `repub_state` for books.

## [0.5] - 2019-06-05

* Revised to deal with Ex Libris SRU formatting changes and our local database revision for June.

## [0.4] - 2019-05-15

* Fixed some issues with uploading IA MARC records
* Added a [notebook](04-manual-addition-to-workflow.ipynb) for just dealing with one book at a time.
    * In the future I want to expand on this idea and roll out tools for interacting with the database.

## [0.3] - 2019-04-09

### Added

* A [third notebook](03-print-sheet-for-pulling-books.ipynb) for generating a printable worksheet for pulling books
* In [notebook 2](02-barcodes-to-database.ipynb), a method to import a barcode list if necessary
* Also in notebook 2, better feedback while running code blocks
* "Digitized at Georgetown University Law Library" note in description metadata for IA
* Launch batch script and an update shell script

### Changed

* MARCXML fixes:
    * Parsing 264 fields was running into an archive.org derive bug, so I'm deleting extra 264 fields after the first
    * While parsing the XML, deleting a few other things (650 subject headings with non-0 second indicators, AVA datafields, LOCAL-marked fields.)
* Bugfixes (all in notebook 2):
    * datetime.datetime error
    * refreshing rows if they've been updated in Google Sheet

## [0.2] - 2019-03-27

### Added

* The second notebook with functions to:
    * work with the Google Sheet directly
    * manage the entire workflow after the books have been pulled and searched on IA:
        * Updating item locations in Alma
        * Generating UIDs
        * Updating digitization database
        * Extracting MARC records from Alma and uploading them to IA
* Credentials are set up on test machine but not in the repo
    * This still needs to be documented

### Removed
 
* Some images and notebook-based directions on working with Alma
    * These have been moved to a Google Doc instead

### Changed

* Miscellaneous minor changes

## [0.1] - 2019-03-19

### Changed

* Fixed a lot of Sheets references and Sheets functionality
* Saving a lookup table while parsing the Alma Analytics output
