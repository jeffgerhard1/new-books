# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 12:07:01 2019

@author: gerhardj

"""

# first step: go through the Alma spreadsheet and pare down to those that have multiple items and at least one checkout

import csv
from unidecode import unidecode
import string
import sys
import os
from tkinter.filedialog import askopenfilename
import pyodbc
from datetime import datetime
import requests
from bs4 import BeautifulSoup


def update_db(cursor, params):  # borrowed from my update db program, 2018
    results = False
    if len(params['searches']) == 1:  # stick with just this for now!
        cursor.execute("""
        SELECT UID
        FROM Item_Entry
        WHERE """ + params['searches'][0][0] + """=?;
        """, params['searches'][0][1])
        try:
            row = cursor.fetchone()
            UID = row.UID
        except:
            return False
        if row:
            for change in params['changes']:
                cursor.execute("""
                UPDATE Item_Entry
                SET """ + change[0] + """=?
                WHERE UID=?;
                """, change[1], UID)
            print(UID, 'updating...')
            results = True
    return results


def makeShortList(almafile, show_results=True, verbose=False):
    ''' For given csv L, return a list of items to pull '''
    # load the mmsids from our database (as copied from the database)
    with open('files\db_item_table_mmsids.txt', 'r') as fh:
        in_our_db = fh.read().splitlines()
    # load up the open library list (as sent to us by IA) and make a list of mmsids
    with open('files\FINAL-gl-inlibrary-matches.tsv', 'r') as fh:
        openlibrary = fh.read().splitlines()
    ol_mmsids = list()
    for line in openlibrary:
        if line.split(' ')[2] not in ol_mmsids:
            ol_mmsids.append(line.split(' ')[2])
    # run a routine to read the csv output from Alma...
    # this file has info on all items so we need to pare it down to just the 
    # items that have multiple copies and at least one circ, plus some other
    # less important criteria
    mmsid_total = 0  # count these as we go
    mult_borrowed = 0  # this is the count of ones that fit our criteria
    alreadyindb = 0  # keep a tally of ones already in our database
    alreadyinol = 0  # and a tally of ones in Open Library list
    RESULTS = list()  # for final results
    with open(almafile, newline='', encoding='utf-8') as fh:
        # set up empty variables to run the loop the first time:
        mmsid = None
        items = 0  # we need to count these per mmsid
        loans = 0  # we need to add these up per mmsid
        descs = list()  # a list of these for each mmsid (descriptions might be "v. 1" or "suppl." or "1997" to distinguish items)
        stuff = list()  # generic list to save the relevant fields that we need for pulling the books
        reader = csv.reader(fh)  # OPEN UP THE CSV!
        fields = next(reader)  # THIS COMMAND WILL READ THE FIRST LINE AS FIELD NAMES
        
        # here is the main loop to parse the csv:
        
        for row in reader:
            if row[fields.index('MMS Id')] == mmsid:  # i.e., if the mmsid is the same as the previous item
                items += 1
                loans += int(row[fields.index('﻿Num of Loans (In House + Not In House)')])
                if row[fields.index('Description')]:  # description field!
                    if row[fields.index('Description')] not in descs:
                        descs.append(row[fields.index('Description')])
            else:  # if the mmsid is different from previous item we will scrutinize the item(s) from the prev. mmsid
                if items > 1 and loans > 0:
                    if verbose is True:
                        print(mmsid, items, loans)
                    if mmsid in in_our_db:
                        alreadyindb += 1
                        add = False
                        if verbose is True:
                            print(mmsid, 'already in digitization db.')
                    elif mmsid in ol_mmsids:
                        alreadyinol += 1
                        add = False
                        if verbose is True:
                            print(mmsid, 'in the Open Library list.')
                    else:
                        add = False
                        if len(descs) == 0:
                            add = True
                        elif len(descs) < 3:
                            if loans > 2 and items > len(descs):
                                add = True
                        elif len(descs) < 6:
                            if items > len(descs) and loans >= len(descs):
                                add = True
                        else:
                            print('Too many volumes or too few loans per volume for', mmsid)  #, stuff[2], stuff[5])
                        if add is True:
                            mult_borrowed += 1
                            RESULTS.append(stuff)
                loans = int(row[fields.index('﻿Num of Loans (In House + Not In House)')])
                items = 1
                mmsid = row[fields.index('MMS Id')]
                stuff = [row[fields.index('Permanent Call Number')],
                         row[fields.index('Location Code')], 
                         row[fields.index('MMS Id')], 
                         row[fields.index('Begin Publication Date')], 
                         row[fields.index('Edition')], 
                         row[fields.index('Title')], 
                         row[fields.index('Author')]]
                descs = list()
                mmsid_total += 1
                if row[fields.index('Description')]:
                    descs.append(row[fields.index('Description')])
    if show_results is True:
        print('Ok! Here are some results:')
        print('Of', mmsid_total, 'MMSIDs,')
        print('\t', alreadyindb, 'already in our database.')
        print('\t', alreadyinol, 'already in the Open Library list')
        print('\t leaving', mult_borrowed, 'to try to scan.')
    return RESULTS


def getDataForDb(almafile, barcodes, proj='Multiple Copies'):
    ''' take list of barcodes, generate uids, return the bib and item info we need '''
    bibs = list()
    mmsids = list()
    items = list()
    itemdata = list()
    bibdata = list()
    with open(almafile, newline='', encoding='utf-8') as fh:
        reader = csv.reader(fh)  # OPEN UP THE CSV!
        fields = next(reader)  # THIS COMMAND WILL READ THE FIRST LINE AS FIELD NAMES
        for row in reader:
            if row[fields.index('Barcode')] in barcodes:
                # have we done this bib id yet?
                if row[fields.index('MMS Id')] not in mmsids:
                    bibdata = [row[fields.index('MMS Id')],
                               row[fields.index('Author')],
                               row[fields.index('Title')],
                               row[fields.index('Publication Place')],
                               row[fields.index('Publisher')],
                               row[fields.index('Begin Publication Date')],
                               row[fields.index('Edition')],
                               row[fields.index('Location Code')],
                               row[fields.index('Permanent Call Number')],
                               proj
                            ]
                    bibs.append(bibdata)
                uid = buildUID(fields, row)
                itemdata = [uid,
                            row[fields.index('MMS Id')],
                            row[fields.index('Barcode')],
                            row[fields.index('Description')],
                            row[fields.index('Item Copy Id')],
                            proj]
                items.append(itemdata)
    return bibs, items


def buildUID(fields, row):
    uid = buildIDchunk(row[fields.index('Title')].strip(), 7)  + '_'
    if row[fields.index('Author')]:
        uid += buildIDchunk(row[fields.index('Author')].strip(), 3)  + '_'
    else:
        uid += 'xxxx_'
    if row[fields.index('Begin Publication Date')]:
        uid += str(row[fields.index('Begin Publication Date')]) + '_'
    else:
        uid += 'xxxx_'
    if row[fields.index('Description')]:
        volinfo = row[fields.index('Description')].strip()
        volinfo = volinfo.replace('v.', '').replace('vol.', '').replace('part', '').replace('pt.', '').strip()
        if volinfo.isdigit():
            uid += volinfo.zfill(2) + '_'
        else:
            uid += buildIDchunk(volinfo, 2) + '_'
    else:
        uid += '00_'
    if row[fields.index('Barcode')]:
        uid += row[fields.index('Barcode')].strip()[-4:]
    else:
        uid += 'xxxx'
    return uid

def buildIDchunk(x, l):
    x = x.translate(str.maketrans('', '', string.punctuation)) # see http://stackoverflow.com/questions/34293875/how-to-remove-punctuation-marks-from-a-string-in-python-3-x-using-translate
    x = unidecode(x).strip().lower().replace(' ','')
    x = x.ljust(l, 'x')
    return x[:l]


def addToDb(bibs, items):
    add = False
    cnxn = pyodbc.connect('DSN=Digitization;Trusted_Connection=Yes;APP=Python;DATABASE=digitization')
    cursor = cnxn.cursor()
    bibheaders = ['Bib_ID', 'Author', 'Title', 'Pub_Place', 'Imprint', 'PubYear', 'Edition', 'Location', 'CallNumber', 'Project']
    itemheaders = ['UID', 'Bib_ID', 'Barcode', 'Volume', 'Copy', 'Project']
    # START WITH BIBS
    params = list()
    for bib in bibs:
        # check to see if in db already!
        # if not insert
        cursor.execute("""
        SELECT Author
        FROM Bib_Entry
        WHERE Bib_ID=?;
        """, bib[0])
        row = cursor.fetchone()
        if row:
            print(bib[2], bib[0], 'already in DB!!')
            add = False
        else:
            add = True
        if add is True:
            print('Adding BIB', bib[2], 'to db.')
            params.append(tuple(bib))
    updatequery = 'INSERT INTO Bib_Entry('
    updatequery += ', '.join(bibheaders)
    updatequery += ') values ('
    updatequery += '?, '*(len(bibheaders) -1) + '?'
    updatequery += ')'

#     print(updatequery)
    cursor.executemany(updatequery, params)
    cursor.commit()

    # NEXT DO ITEMS

    print()
    params = list()
    events = list()
    for item in items:
        # check to see if in db already!
        # if not insert
        cursor.execute("""
        SELECT Bib_ID
        FROM Item_Entry
        WHERE Barcode=?;
        """, item[2])
        row = cursor.fetchone()
        if row:
            print(item[0], item[2], 'already in DB!!')
            add = False
        else:
            add = True
        if add is True:
            print('Adding ITEM', item[0], 'to db.')
            params.append(tuple(item + [1]))  # the 1 is for (required) item type
            events.append(tuple([item[0], 56, datetime.now()]))
    updatequery2 = 'INSERT INTO Item_Entry('
    updatequery2 += ', '.join(itemheaders)
    updatequery2 += ', ItemEntryTypeID) values ('
    updatequery2 += '?, '*len(itemheaders) + '?'
    updatequery2 += ')'

    cursor.executemany(updatequery2, params)
    cursor.commit()
   
    loc_conn_str = (
    r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
    r'DBQ=E:\j\Documents\test.accdb;'
    )
    loc_cnxn = pyodbc.connect(loc_conn_str)
    loc_cursor = loc_cnxn.cursor()
    updatequery3 = '''INSERT INTO Item_Event_History(Item_UID, Event_type, Event_date) values (?, ?, ?)'''
    loc_cursor.executemany(updatequery3, events)
    loc_cursor.commit()
    return params, events


def grabMarcRec(mmsid):
    url = 'https://wrlc-gulaw.alma.exlibrisgroup.com/view/sru/01WRLC_GUNIVLAW?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.mms_id='
    r = requests.get(url + mmsid)
    soup = BeautifulSoup(r.text)
    return soup.find(xmlns="http://www.loc.gov/MARC21/slim")




if __name__ == "__main__":
   # print(sys.argv)
    path = os.path.dirname(os.path.realpath(__file__))
    if len(sys.argv) == 3:
        if sys.argv[1] == 'assess':
            # save a stripped-down version of the csv into an output file we can use to pull books:
            shortlist = makeShortList(sys.argv[2])
            with open(os.path.join(path, 'results2.csv'), 'w', newline='', encoding='utf-8') as fh:
                wr = csv.writer(fh)
                for _ in shortlist:
                    wr.writerow(_)
    elif len(sys.argv) == 4:
        if sys.argv[1] == 'bibitem':
            print('trying to snag bib and item info')
            with open(sys.argv[3], 'r', encoding='utf-8') as fh:
                barcodes = fh.read().splitlines()
            bibs, items = getDataForDb(sys.argv[2], barcodes)
            with open(os.path.join(path, 'bibs.csv'), 'w', newline='', encoding='utf-8') as fh:
                wr = csv.writer(fh)
                for _ in bibs:
                    wr.writerow(_)
            with open(os.path.join(path, 'items.csv'), 'w', newline='', encoding='utf-8') as fh:
                wr = csv.writer(fh)
                for _ in items:
                    wr.writerow(_)
        if sys.argv[1] == 'database':
            print('Trying to update database \n\t(really just testing for now)')
            with open(sys.argv[2], 'r', encoding='utf-8') as fh:
                bibs = list()
                items = list()
                bibreader = csv.reader(fh)
                for row in bibreader:
                    bibs.append(row)
                # bibs = fh.read().splitlines()
            with open(sys.argv[3], 'r', encoding='utf-8') as fh2:
                itemreader = csv.reader(fh2)
                for row in itemreader:
                    items.append(row)
                # items = fh.read().splitlines()
            x, y = addToDb(bibs, items)

            
                

