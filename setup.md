# Setup

This suite of tools requires Python and some modules. It's intended to be used as Jupyter notebooks, but you can probably also use the Python code in other ways. 

So far this is only tested on Windows machines. It may also work on Mac or Linux ones. I have made this a __Python 3__ product only. If you need Python 2 support, let me know and I can possibly adjust the code.

## Set up Python and git:

For installing Python on Windows, I recommend using [Miniconda](https://docs.conda.io/en/latest/miniconda.html). 

To get these files locally, use `git`. If not installed, you should install [Git for Windows](https://gitforwindows.org/). 

I'm assuming that the Miniconda python.exe directory is in your computer's PATH. 

## Set up the repository locally:

Choose or create a directory like "repos" on your computer. Open `git-bash` there and run the following to clone this repo (or use GUI alternatives).

`git clone https://jeffgerhard1@bitbucket.org/jeffgerhard1/new-books.git`

## Install required Python modules:

Now, open a Windows command line terminal in the folder for the __new-books__ repo. Run the following to install required modules:

`pip install -r requirements.txt`

This will go through the requirements.txt document that comes with the repo, and install the packages listed.

## Download credentials files:

These are not bundled in this repository. The credentials are identical to the ones needed for the [D.C. Circuit] notebooks. Download them from our Team Drive at 

Digital Initiatives\accounts and credentials\Google credentials for programs\credentials

Just copy the entire "credentials" directory into the same folder where this document is sitting (so, alongside of the `files` directory).

### Logging in with departmental account

If you want to do every step of this process, including adding `globallibraries` collection to our items, you will need the credentials for the departmental account.

You can set this up by running the `00-setup-ia-dept-user` notebook, entering the email address and password. You should only need to do this once.

## Launching the notebooks:

If you've installed Miniconda (or Anaconda, its larger variation), you may need to activate the `conda` environment. You can run the commands `activate` and then `jupyter lab` to start the Jupyter environment, like this:

    E:\j\Documents\git_repos\new-books> activate
    (base) E:\j\Documents\git_repos\new-books> jupyter lab

Alternatively, run the batch script included. It will attempt to detect the python executable file, so it should work even if your PATH variables are not set up.
