# -*- coding: utf-8 -*-
"""

Straightforward method to upload a MARC record to IA, if you have a UID, BibID, and volume info.


"""
import requests
from bs4 import BeautifulSoup
import os
import internetarchive as ia


tempmarcs = os.path.join(r'C:\temp', 'marcrecordtemp')

def grabMarcRec(mmsid, tempmarcs):
    url = 'https://wrlc-gulaw.alma.exlibrisgroup.com/view/sru/01WRLC_GUNIVLAW?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.mms_id='
    r = requests.get(url + mmsid)
    soup = BeautifulSoup(r.text, 'xml')
    #marcxml = str(soup.find(xmlns="http://www.loc.gov/MARC21/slim"))
    marcxml = str(soup.record.record)
    # clean-up routine:
#    found264s = 0
    newlines = list()   
    for line in marcxml.splitlines():
        if '<record' in line:
            newlines.append(r'<record xmlns="http://www.loc.gov/MARC21/slim">')
        elif '<leader' in line or '<controlfield' in line or r'</record' in line:
            newlines.append(line)
        else:
            if '<datafield' in line:
                newdatafield = list()
                skip = False
            newdatafield.append(line)
#            if 'tag="264"' in line:
#                found264s += 1
#                if found264s > 1:
#                    skip = True
#                    print('deleting an extra 264 record.')
            if 'tag="650"' in line or 'tag="651"' in line or 'tag="600"' in line:
                if 'ind2="0"' not in line:
                    skip = True
            if r'<subfield code="9">LOCAL</subfield>' in line or 'tag="AVA"' in line:
                skip = True            
            if r'</datafield>' in line:
                if skip is False:
                    newlines += newdatafield
    newxml = '\n'.join(newlines)    
    return newxml

def uploadMARC(uid, mmsid, volume=None):
    global tempmarcs
    md = dict()
    md['collection'] = 'georgetown-university-law-library-rr'
    # md['scanningcenter'] = 'tt_georgetown'
    md['noindex'] = 'true'
    md['neverindex'] = 'true'
    md['description'] = 'Digitized at Georgetown University Law Library'
    md['mediatype'] = 'texts'
    md['sponsor'] = 'Georgetown University Law Library'
    md['contributor'] = 'Georgetown University Law Library'
    md['gulawlib-identifier'] = mmsid
    if volume:
        md['volume'] = volume
    if uid:
        xml = grabMarcRec(mmsid, tempmarcs)
        xmlfilename = uid + '_marc.xml'
        marcfilename = os.path.join(tempmarcs, xmlfilename)
        with open(marcfilename, 'w', encoding='utf-8') as fh:
            fh.write(xml)
        print('uploading MARC record for', uid)
        r = ia.upload(uid, files=[marcfilename], metadata=md, verbose=True, checksum=True)        
        return r[0].status_code
    else:
        print('Error on', mmsid)


def prepForScribe(uid):
    item = ia.get_item(uid)
    if 'collection' in item.metadata:
        colls = item.metadata['collection']
        if 'globallibraries' not in colls:
            colls.append('globallibraries')
            r = ia.modify_metadata(uid, dict(collection=colls,
                                             scanningcenter='tt_georgetown',
                                             repub_state=-1))
            return r

if __name__ == '__main__':
    print('Ready to upload a MARC record to IA\n')
    volume = None
    uid = input('Enter the UID:').strip()
    bibid = input('Enter the MMSID:').strip()
    v = input('Enter volume info (or hit enter if none)').strip()
    if v:
        volume = v
    uploadMARC(uid, bibid, volume=volume)
